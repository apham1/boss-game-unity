﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour {

    [SerializeField] Transform playerTransform;

	// Use this for initialization
	void Start () {
        MoveToPlayer();
	}
	
	// Update is called once per frame
	void Update () {
        MoveToPlayer();
    }

    void MoveToPlayer() {
        if (playerTransform) {
            transform.position = playerTransform.position - new Vector3(0f, 0f, 12f);
        }
    }
}
