﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    [SerializeField] int bulletDamage = 5;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision) {
        Destroy(gameObject);

        Mortal mortal = collision.gameObject.GetComponent<Mortal>();
        Enemy enemy = collision.gameObject.GetComponent<Enemy>();
        if (mortal && enemy) {
            mortal.changeHealth(-bulletDamage);
        }
    }
}
