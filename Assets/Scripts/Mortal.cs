﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mortal : MonoBehaviour {

    [SerializeField] int health = 100;

    bool alive;
    int maxHealth;

	// Use this for initialization
	void Start () {
        alive = true;
        maxHealth = health;
	}
	
	// Update is called once per frame
	void Update () {
		if (health <= 0f) {
            alive = false;
        }
	}

    public void changeHealth(int change) {
        health += change;
    }

    public int getHealth() {
        return health;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public bool isAlive() {
        return alive;
    }
}
