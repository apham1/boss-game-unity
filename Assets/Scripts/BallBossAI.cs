﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallBossAI : MonoBehaviour {
    enum Direction {
        LEFT,
        RIGHT
    }

    [SerializeField] Transform playerTransform;
    [SerializeField] float attackCooldown = 5f;
    [SerializeField] float spinAttackSpeed = 20f;
    [SerializeField] float spinAttackArchSpeed = 20f;
    [SerializeField] float initialSpinTime = 3f;
    [SerializeField] GameObject explosionPrefab;
    [SerializeField] GameObject shockwavePrefab;
    [SerializeField] GameObject healthBarPrefab;
    [SerializeField] Transform UITransform;

    Rigidbody rigidBody;
    Animator animator;
    Mortal mortal;
    float attackTimeStamp;
    bool isDying;
    GameObject healthBar;
    Image healthBarFill;

	// Use this for initialization
	void Start () {
        rigidBody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        mortal = GetComponent<Mortal>();
        attackTimeStamp = Time.time + 2f;
        isDying = false;
        healthBar = Instantiate(healthBarPrefab, UITransform);
        healthBarFill = healthBar.GetComponentsInChildren<Image>(true)[1];
    }
	
	// Update is called once per frame
	void Update () {
        updateHealthBar();
        if (mortal.isAlive()) {
            if (attackTimeStamp <= Time.time && playerTransform) {
                Spin();
            }
        } else {
            if (!isDying) {
                isDying = true;
                StopAllCoroutines();
                rigidBody.constraints = RigidbodyConstraints.FreezeAll;
                StartCoroutine(Die(3f));
            }
        }
    }

    void Spin() {
        attackTimeStamp = Time.time + initialSpinTime + attackCooldown;
        Direction attackDirection = Direction.LEFT;

        if (playerTransform.position.x < transform.position.x) {
            animator.SetTrigger("StartSpinChargeLeft");
        } else {
            attackDirection = Direction.RIGHT;
            animator.SetTrigger("StartSpinChargeRight");
        }

        switch (Random.Range(0, 2)) {
            case 0:
                StartCoroutine(SpinAttack(attackDirection, initialSpinTime));
                break;
            case 1:
                StartCoroutine(SpinArchAttack(attackDirection, initialSpinTime));
                break;
        }
    }

    IEnumerator SpinAttack(Direction direction, float waitTime) {
        yield return new WaitForSeconds(waitTime);
        rigidBody.constraints = RigidbodyConstraints.None;
        if (mortal.isAlive()) {
            if (direction == Direction.LEFT) {
                rigidBody.velocity = new Vector3(-spinAttackSpeed, 0f, 0f);
            } else {
                rigidBody.velocity = new Vector3(spinAttackSpeed, 0f, 0f);
            }
            animator.SetTrigger("StartSpin");
            StartCoroutine(ReturnToIdle(2f));
        }
    }

    IEnumerator SpinArchAttack(Direction direction, float waitTime) {
        yield return new WaitForSeconds(waitTime);
        rigidBody.constraints = RigidbodyConstraints.None;
        if (mortal.isAlive()) {
            if (direction == Direction.LEFT) {
                rigidBody.velocity = new Vector3(-spinAttackSpeed, spinAttackArchSpeed, 0f);
            } else {
                rigidBody.velocity = new Vector3(spinAttackSpeed, spinAttackArchSpeed, 0f);
            }
            animator.SetTrigger("StartSpin");
            StartCoroutine(ReturnToIdle(2f));
        }
    }

    IEnumerator Die(float waitTime) {
        animator.SetTrigger("ReturnToIdle");

        GameObject shockwave = Instantiate(shockwavePrefab, transform.position, new Quaternion());
        Destroy(shockwave, waitTime);

        yield return new WaitForSeconds(waitTime);

        GameObject explosion = Instantiate(explosionPrefab, transform.position, new Quaternion());
        Destroy(explosion, 1f);

        Destroy(this.gameObject);
    }


    IEnumerator ReturnToIdle(float waitTime) {
        yield return new WaitForSeconds(waitTime);
        rigidBody.constraints = RigidbodyConstraints.FreezePosition;
        rigidBody.velocity = new Vector3(0f, 0f, 0f);
        animator.SetTrigger("ReturnToIdle");
    }

    void updateHealthBar() {
        healthBarFill.fillAmount = (float)mortal.getHealth() / mortal.getMaxHealth();
    }

    private void OnCollisionEnter(Collision collision) {
        Wall wall = collision.gameObject.GetComponent<Wall>();
        if (wall) {
            rigidBody.constraints = RigidbodyConstraints.FreezePositionX;
            rigidBody.velocity = new Vector3(0f, rigidBody.velocity.y, 0f);
        }
    }
}
