﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour {
    enum Direction {
        LEFT,
        RIGHT
    }

    [SerializeField] float playerSpeed = 7.5f;
    [SerializeField] float playerJumpSpeed = 10f;
    [SerializeField] int playerMaxJumps = 2;
    [SerializeField] GameObject bulletPrefab;
    [SerializeField] float bulletSpeed = 20f;
    [SerializeField] float bulletCooldown = 0.2f;
    [SerializeField] Transform bulletLeftSpawn;
    [SerializeField] Transform bulletRightSpawn;
    [SerializeField] Transform bulletTopSpawn;
    [SerializeField] Transform bulletTopLeftSpawn;
    [SerializeField] Transform bulletTopRightSpawn;
    [SerializeField] float invincibleTimeOnHit = 2f;
    [SerializeField] GameObject healthBarPrefab;
    [SerializeField] Transform UITransform;

    Rigidbody rigidBody;
    MeshRenderer meshRenderer;
    Animator animator;
    Mortal mortal;
    float distanceToGround;
    Vector3 input;
    float bulletTimeStamp;
    Direction direction;
    int currentJumps;
    bool isDying;
    bool isInvincible;
    GameObject healthBar;
    List<Image> hearts = new List<Image>();
    

    // Use this for initialization
    void Start() {
        rigidBody = GetComponent<Rigidbody>();
        meshRenderer = GetComponent<MeshRenderer>();
        animator = GetComponent<Animator>();
        mortal = GetComponent<Mortal>();
        distanceToGround = GetComponent<Collider>().bounds.extents.y;
        bulletTimeStamp = Time.time;
        currentJumps = 0;
        isDying = false;
        isInvincible = false;
        ignoreEnemyCollision(false);

        healthBar = Instantiate(healthBarPrefab, UITransform);
        foreach (Transform heartBGTransform in healthBar.transform) {
            foreach (Transform heartFillTransform in heartBGTransform) {
                hearts.Add(heartFillTransform.gameObject.GetComponent<Image>());
            }
        }
    }
	
	// Update is called once per frame
	void Update() {
        RestartLevel();
        if (mortal.isAlive()) {
            HandleMove();
            Jump();
            Fire();

            doBlinkAnimation();
        } else if (!isDying) {
            isDying = true;
            StartCoroutine(Die(2f));
        }
    }

    private void FixedUpdate() {
        if (mortal.isAlive()) {
            Walk();
        }
    }

    IEnumerator Die(float waitTime) {
        animator.SetTrigger("Die");
        yield return new WaitForSeconds(waitTime);
        Destroy(this.gameObject);
    }

    void Jump() {
        if (Input.GetKeyDown(KeyCode.Space) && (IsGrounded() || currentJumps < playerMaxJumps)) {
            rigidBody.velocity = new Vector3(rigidBody.velocity.x, playerJumpSpeed);
            currentJumps++;
        }
    }

    void Walk() {
        float speedThisFrame = playerSpeed * Time.fixedDeltaTime;
        rigidBody.MovePosition(rigidBody.position + input * speedThisFrame);
    }

    void Fire() {
        if (Input.GetKey(KeyCode.Mouse0) && bulletTimeStamp <= Time.time) {
            bulletTimeStamp = Time.time + bulletCooldown;

            GameObject bullet = null;
            if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.A)) {
                bullet = Instantiate(bulletPrefab, bulletTopLeftSpawn.position, new Quaternion());
                bullet.GetComponent<Rigidbody>().velocity = Vector3.Normalize(new Vector3(-2f, 1f, 0f)) * bulletSpeed;
            } else if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.D)) {
                bullet = Instantiate(bulletPrefab, bulletTopRightSpawn.position, new Quaternion());
                bullet.GetComponent<Rigidbody>().velocity = Vector3.Normalize(new Vector3(2f, 1f, 0f)) * bulletSpeed;
            } else if (Input.GetKey(KeyCode.W)) { 
                bullet = Instantiate(bulletPrefab, bulletTopSpawn.position, new Quaternion());
                bullet.GetComponent<Rigidbody>().velocity = Vector3.Normalize(new Vector3(0f, 1f, 0f)) * bulletSpeed;
            } else {
                if (direction == Direction.LEFT) {
                    bullet = Instantiate(bulletPrefab, bulletLeftSpawn.position, new Quaternion());
                    bullet.GetComponent<Rigidbody>().velocity = Vector3.Normalize(new Vector3(-1f, 0f, 0f)) * bulletSpeed;
                } else if (direction == Direction.RIGHT) {
                    bullet = Instantiate(bulletPrefab, bulletRightSpawn.position, new Quaternion());
                    bullet.GetComponent<Rigidbody>().velocity = Vector3.Normalize(new Vector3(1f, 0f, 0f)) * bulletSpeed;
                }
            }
            Physics.IgnoreCollision(bullet.GetComponent<Collider>(), GetComponent<Collider>());

            Destroy(bullet, 2.0f);
        }
    }

    void HandleMove() {
        float horizontalThrow = Input.GetAxis("Horizontal");
        input = new Vector3(horizontalThrow, 0f, 0f);

        if (horizontalThrow < 0f) {
            direction = Direction.LEFT;
        } else if (horizontalThrow > 0f) {
            direction = Direction.RIGHT;
        }
    }

    bool IsGrounded() {
        return Physics.Raycast(transform.position - new Vector3(GetComponent<Collider>().bounds.extents.x, 0, 0), -Vector3.up, distanceToGround + 0.1f)
            || Physics.Raycast(transform.position, -Vector3.up, distanceToGround + 0.1f)
            || Physics.Raycast(transform.position + new Vector3(GetComponent<Collider>().bounds.extents.x, 0, 0), -Vector3.up, distanceToGround + 0.1f);
    }

    private void OnCollisionEnter(Collision collision) {
        if (IsGrounded()) {
            currentJumps = 0;
        }

        handleEnemyCollision(collision);
    }

    private void OnCollisionStay(Collision collision) {
        handleEnemyCollision(collision);
    }

    IEnumerator StartInvincibility(float waitTime) {
        isInvincible = true;
        ignoreEnemyCollision(true);

        yield return new WaitForSeconds(waitTime);

        isInvincible = false;
        ignoreEnemyCollision(false);
    }

    void handleEnemyCollision(Collision collision) {
        Enemy enemy = collision.gameObject.GetComponent<Enemy>();
        if (enemy && !isInvincible) {
            getHit(collision, enemy.getDamageAmount());
        }
    }

    void getHit(Collision collision, int damage) {
        StartCoroutine(StartInvincibility(invincibleTimeOnHit));
        mortal.changeHealth(-damage);
        updateHealthBar();
    }

    void ignoreEnemyCollision(bool flag) {
        //Layer 8=Player, Layer 9=Enemy
        Physics.IgnoreLayerCollision(8, 9, flag);
    }

    void doBlinkAnimation() {
        if (isInvincible) {
            toggleVisibility();
        } else {
            meshRenderer.enabled = true;
        }
    }

    void toggleVisibility() {
        meshRenderer.enabled = !meshRenderer.enabled;
    }

    public void updateHealthBar() {
        for (int i = hearts.Count - 1; i > mortal.getHealth() - 1 && i >= 0; i--) {
            hearts[i].fillAmount = 0f;
        }
    }

    void RestartLevel() {
        if (Input.GetKeyDown(KeyCode.R)) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
