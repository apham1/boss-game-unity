﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Heart : MonoBehaviour {

    bool isActive;
    Animator animator;
    public Image heartFill;

	// Use this for initialization
	void Start () {
        isActive = true;
        animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        setState();
	}

    void setState() {
        if (heartFill) {
            if (heartFill.fillAmount == 0.0f && isActive) {
                isActive = false;
                animator.SetTrigger("ToEmpty");
            } else if (heartFill.fillAmount > 0.0f && !isActive) {
                isActive = true;
                animator.SetTrigger("ToActive");
            }
        }
    }
}
